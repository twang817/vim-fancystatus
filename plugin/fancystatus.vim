if exists('g:loaded_fancystatus_plugin')
    finish
endif
let g:loaded_fancystatus_plugin = 1

let laststatus = 2

function! FancyStatus_BufCount()
    if !exists('s:fancystatus_bufcount')
        let s:fancystatus_bufcount = len(filter(range(1, bufnr('$')), 'buflisted(v:val)'))
    endif
    return s:fancystatus_bufcount
endfunction

function FancyStatus_MixedIndentWarning()
    if !exists('b:fancystatus_mixedindentwarning')
        let b:fancystatus_mixedindentwarning = ''
        if !&modifiable
            return b:fancystatus_mixedindentwarning
        endif
        let tabs = search('^\t', 'nw') != 0
        let spaces = search('^ ', 'nw') != 0
        if tabs && spaces
            let b:fancystatus_mixedindentwarning = g:fancystatus_mixedindentwarning_mixedmsg
        elseif (spaces && !&et) || (tabs && &et)
            let b:fancystatus_mixedindentwarning = '[&et]'
        endif
    endif
    return b:fancystatus_mixedindentwarning
endfunction

function! FancyStatus_TrailingWhitespaceWarning()
    if !exists('b:fancystatus_trailingwhitespacewarning')
        let b:fancystatus_trailingwhitespacewarning = ''
        if !&modifiable
            return b:fancystatus_trailingwhitespacewarning
        endif
        if search('\s\+$', 'nw') != 0
            let b:fancystatus_trailingwhitespacewarning = '[\s]'
        endif
    endif
    return b:fancystatus_trailingwhitespacewarning
endfunction

if !exists('g:fancystatus_show_buffer_count')
    let g:fancystatus_show_buffer_count = 1
endif

if !exists('g:fancystatus_show_filename_relative')
    let g:fancystatus_show_filename_relative=1
endif

if !exists('g:fancystatus_tagbar_show_currenttag')
    let g:fancystatus_tagbar_show_currenttag = 1
endif

if !exists('g:fancystatus_fugitive_show_current_branch')
    let g:fancystatus_fugitive_show_current_branch = 1
endif

if !exists('g:fancystatus_warn_mixed_indent')
    let g:fancystatus_warn_mixed_indent = 1
endif

if !exists('g:fancystatus_warn_trailing_whitespace')
    let g:fancystatus_warn_trailing_whitespace = 1
endif

" buffer number : buffer count
if g:fancystatus_show_buffer_count
    set statusline=[%{FancyStatus_BufCount()}\:%n]\ %<
    augroup fancystatus_bufcount
        autocmd!
        autocmd BufAdd,BufDelete * unlet! s:fancystatus_bufcount
    augroup END
else
    set statusline=[%n]\ %<
endif

" filename (relative or tail)
if g:fancystatus_show_filename_relative
    set statusline+=%1*[%f]%*
else
    set statusline+=%1*[%t]%*
endif

" flags (h:help:[help], m:modified:[+][-], r:readonly:[RO], w:window:[PREVIEW])
set statusline+=%2*%h%m%r%w%*

" filetype
set statusline+=%y

" file format : file encoding
set statusline+=[%{&ff}:%{strlen(&fenc)?&fenc:'-'}]

" [tagbar] current tag
if g:fancystatus_tagbar_show_currenttag
    set statusline+=%4*%{tagbar#currenttag('[%s]','','f')}%*
endif

" separate left/right aligned
set statusline+=%=

" (-:left align 14:minwid, l:line, L:nLines, c:column)
set statusline+=%-14(\ L%l/%L:C%c(%v)\ %)

" scroll percent
set statusline+=%P

" (b:num, B:hex) of char under cursor
set statusline+=%9(\ \%b/0x\%B%)

" [fugitive] current branch
if g:fancystatus_fugitive_show_current_branch
    set statusline+=\ %4*%{fugitive#statusline()}%*
endif

" warn when mixed indenting is detected
if g:fancystatus_warn_mixed_indent
    set statusline+=%3*%{FancyStatus_MixedIndentWarning()}%*
    augroup fancystatus_mixedindentwarning
        autocmd!
        autocmd CursorHold,BufWritePost * unlet! b:fancystatus_mixedindentwarning
    augroup END
endif

" warn when trailing whitespace is detected
if g:fancystatus_warn_trailing_whitespace
    set statusline+=%3*%{FancyStatus_TrailingWhitespaceWarning()}%*
    augroup fancystatus_trailingwhitespacewarning
        autocmd!
        autocmd CursorHold,BufWritePost * unlet! b:fancystatus_trailingwhitespacewarning
    augroup END
endif
